%%% generate face audio test mat (simulate real steaming case)

close all;clc;clear;

% toggles off print info and waitbar
mirverbose(0);
mirwaitbar(0);
mirtemporary(0);

run_on_sever = 0;           % 1: run on sever, 0: otherwise
audio_normalization = 0;    % 1 for yes, 0 otherwise

if run_on_sever == 0
    %% data params
    folder_base = 'data';    % big bang theory
    video_name = 'S01E06';
    folder_faces_base = 'data/faces/6';
    folder_to_save = 'data/real_simulate_data';
    num_frames_shift_both_ends = 10; % shift both frame idx in each subtitle to filter noise
    num_classes = 5;
    names = {'sheldon', 'leonard', 'howard', 'raj', 'penny'};
    sz_template = [50 50];      % [height, width], all imgs will resize to this size
    
    
    %% load audio data directly from audio file
    audioFile = [folder_base '/' video_name '.wav'];
    audioData = miraudio(audioFile, 'sampling', 16000); % resample to 16kHz
    save([folder_base '/extracted_features/' video_name '/audioData-16kHz.mat'], 'audioData');
    close all;


    %% load audio data from saved mat
    load([folder_base '/extracted_features/' video_name '/audioData-16kHz.mat']);
    numAudioFrames = size(mirgetdata(audioData), 1);
else
    %% data params - on server
    folder_base = 'D:\FtpDir\Shared\Yongtao\==speaker-naming-on-bigbang/';    % big bang theory
    video_name = 'S01E06';
    folder_faces_base = 'D:\FtpDir\Shared\Yongtao\==speaker-naming-on-bigbang/6';
    folder_to_save = 'D:\FtpDir\Shared\Yongtao\==speaker-naming-on-bigbang/';
    num_frames_shift_both_ends = 10; % shift both frame idx in each subtitle to filter noise
    num_classes = 5;
    names = {'sheldon', 'leonard', 'howard', 'raj', 'penny'};
    sz_template = [50 50];      % [height, width], all imgs will resize to this size

    
    % %% load audio data directly from audio file
    % audioFile = [folder_base '/' video_name '.wav'];
    % audioData = miraudio(audioFile, 'sampling', 16000); % resample to 16kHz
    % save([folder_base '/audioData-16kHz.mat'], 'audioData');
    % close all;


    %% load audio data from saved mat
    load([folder_base '/audioData-16kHz.mat']);
    numAudioFrames = size(mirgetdata(audioData), 1);
end


%% num of video frames
if strcmp(video_name, 'S01E01')
    numVideoFrames = 33956;
elseif strcmp(video_name, 'S01E02')
    numVideoFrames = 29756;
elseif strcmp(video_name, 'S01E03')
    numVideoFrames = 31058;
elseif strcmp(video_name, 'S01E04')
    numVideoFrames = 28923;
elseif strcmp(video_name, 'S01E05')
    numVideoFrames = 28596;
elseif strcmp(video_name, 'S01E06')
    numVideoFrames = 29436;
end


%% prepare audio
fileSpeakerGT = sprintf('%s/%s - speaker_gt_file - single speakers.txt', folder_base, video_name);
numLines = 1000; % larger is fine
[startVideoFrameIndex, endVideoFrameIndex, label] = textread(fileSpeakerGT, '%*s %d %d %d', numLines); 

numLinesReal = size(startVideoFrameIndex, 1);
idx_track = 0;
face_audio = cell(numLinesReal, 1);
for i = 1:numLinesReal  
    print_progress(1, numLinesReal, i, 'extracting: ');
    
    if (label(i) >= num_classes || label(i)<0)
        continue;
    end
    
    realStart = startVideoFrameIndex(i,1)+num_frames_shift_both_ends;
    realEnd = endVideoFrameIndex(i,1)-num_frames_shift_both_ends;
    
    if (realStart > realEnd)
        continue;
    end
    
    %%% extract audio features here
    % load roi audio
    startAudioFrameIndex = fix(numAudioFrames * realStart / numVideoFrames);
    endAudioFrameIndex   = fix(numAudioFrames * realEnd   / numVideoFrames);
    if startAudioFrameIndex+400 > endAudioFrameIndex
        continue;
    end
    audioRoi = miraudio(audioData, 'Extract', startAudioFrameIndex, endAudioFrameIndex, 'sp');    
    % split into different frames, 20ms as one, half overlapping
    frames = mirframe(audioRoi, 'Length', 0.02, 's'); 
    % compute 25d mfcc + delta + delta, all 75d in total
    mfcc = mirgetdata(mirmfcc(frames, 'Rank', 1:25));
    mfcc_d1 = mirgetdata(mirmfcc(frames, 'Rank', 1:25, 'Delta', 1));
    mfcc_d2 = mirgetdata(mirmfcc(frames, 'Rank', 1:25, 'Delta', 2));
    % merge them together
    samples = [mfcc(:, 9:end); mfcc_d1(:, 5:end); mfcc_d2];   
    if (isempty(samples))
        continue;
    end
    if audio_normalization == 1
        % normalize
        samples = samples - mean(samples(:));
        samples = samples / std(samples(:));
    end
    % save to final data, only use the 1/3 one
    audio = samples(:, floor(end/3));
    
    %% extract faces within this range
    idx_track_face = 0;
    for j=realStart:realEnd      
        % load face data - big bang
        for idx=1:10 % assume have max 10 faces in a frame
            for idx_names=1:length(names) % search for each people
                path_face = sprintf('%s/%s/%06d_%02d.jpg', folder_faces_base, names{idx_names}, j, idx-1);
                if exist(path_face)~=0 % find a face
                    face = im2double(imresize(imread(path_face), sz_template));

                    idx_track_face = idx_track_face + 1;
                    face_audio{idx_track+1, 1}.face(:, :, :, idx_track_face) = face;
                    face_audio{idx_track+1, 1}.face_frame_idx(idx_track_face, 1) = j-realStart;
                end
            end                
        end                             
    end
    
    %% only if have faces in this range
    if idx_track_face~=0
        idx_track = idx_track + 1;

        %% label
        face_audio{idx_track, 1}.label = label(i);

        %% audio
        % mirsave(audioRoi, sprintf('data/big_bang/real_simulate_data/audio/audio_%03d.wav', idx_track));            
        face_audio{idx_track, 1}.audio = audio;
    end 
end


%% remove empty cells
emptyCells = cellfun('isempty', face_audio); 
face_audio(all(emptyCells,2),:) = [];


%% write feature matrix to disk
save(sprintf('%s/test_simulate_no_audio_normalization.mat', folder_to_save), 'face_audio', '-v7.3');