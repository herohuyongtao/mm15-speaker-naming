%%% generate audio feature mat (normalized)

close all;clc;clear;

% toggles off print info and waitbar
mirverbose(0);
mirwaitbar(0);

folder_base = 'data';    % big bang theory
foder_to_save = 'data/extracted_features/audio-merged-456-3/train';
videos = {'S01E04', 'S01E05', 'S01E06'};

% merge data here
audio_data_all_0 = [];
audio_data_all_1 = [];
audio_data_all_2 = [];
audio_data_all_3 = [];
audio_data_all_4 = [];
for i=0:4
    for j=1:length(videos)
        audioFile = sprintf('%s/extracted_features/%s/audio_merged_%d.wav', folder_base, videos{j}, i);
        
        if ~exist(audioFile, 'file')
            continue;
        end
        audioData = miraudio(audioFile);
        
        if i==0
            audio_data_all_0 = [audio_data_all_0; mirgetdata(audioData)];
        elseif i==1
            audio_data_all_1 = [audio_data_all_1; mirgetdata(audioData)];
        elseif i==2
            audio_data_all_2 = [audio_data_all_2; mirgetdata(audioData)];
        elseif i==3
            audio_data_all_3 = [audio_data_all_3; mirgetdata(audioData)];
        elseif i==4
            audio_data_all_4 = [audio_data_all_4; mirgetdata(audioData)];
        end
    end
end

% write feature matrix to disk
audiowrite(sprintf('%s/audio_merged_all_0.wav', foder_to_save), audio_data_all_0, 16000);
audiowrite(sprintf('%s/audio_merged_all_1.wav', foder_to_save), audio_data_all_1, 16000);
audiowrite(sprintf('%s/audio_merged_all_2.wav', foder_to_save), audio_data_all_2, 16000);
audiowrite(sprintf('%s/audio_merged_all_3.wav', foder_to_save), audio_data_all_3, 16000);
audiowrite(sprintf('%s/audio_merged_all_4.wav', foder_to_save), audio_data_all_4, 16000);