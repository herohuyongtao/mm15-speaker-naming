%%% merge all train sub-mats into one, for using in extract_match_classification_train_data.m

clear;clc;close all;

% data params
folder_face = 'data/faces';
str_train_prefix = 'train_';
idx_range_train_submat_face = [1 15];

% ...
for i = idx_range_train_submat_face(1):idx_range_train_submat_face(2)
    print_progress(idx_range_train_submat_face(1), idx_range_train_submat_face(2), i, 'merging: ');
    
    load(sprintf('%s/%s%d.mat', folder_face, str_train_prefix, i));
    
    if i==1
        train = sample;
        label = tag;
    else
        train(:,:,:,size(train,4)+1:size(train,4)+size(sample,4)) = sample;
        label = [label tag];
    end
end

save(sprintf('%s/train_all.mat', folder_face), 'train', 'label', '-v7.3');