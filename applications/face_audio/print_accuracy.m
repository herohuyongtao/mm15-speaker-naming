%%print_accuracy: print detailed accuracy info including confusion matrix info in command window
% -input:
%       estimated_labels: 1xN or Nx1, with values [1,num_classes]
%       true_labels:      1xN or Nx1, with values [1,num_classes]
%       num_classes:      num of classes
% -output: 
%       str:              all accuracy info string, can directly print out by fprintf('%s',str);
%           in meantime, will print 4 infos cin command window, including:
%               1. Overall accuracy
%               2. Detailed accuracy for each class
%               3. Confusion matrix (int, x:predicted label, y:true label)
%               4. Confusion matrix (float, x:predicted label, y:true label)

function str = print_accuracy(estimated_labels, true_labels, num_classes)

str = '';

%% compute confusion matrix, num_count in format x:predicted label, y:true label
cm = zeros(num_classes,num_classes);
for i=1:num_classes
    for j=1:num_classes
        cm(i, j) = length(find(estimated_labels(true_labels==i) == j));
    end
end


%% print
% print overall accuracy
num_total = length(estimated_labels);
num_correct = length(find(estimated_labels == true_labels));
str = sprintf('%s\nOverall accuracy: %.3f%%\n', str, 100*num_correct/num_total);

% print accuracy for each class
str = sprintf('%s\nDetailed accuracy for each class:\n', str);
for i=1:num_classes
    str = sprintf('%s%2d\t : %.3f%%\n', str, i, 100*cm(i,i)/sum(cm(i,:)));
end

% print int CM here
str = sprintf('%s\nConfusion matrix (int, x:predicted label, y:true label):\n', str);
for i=1:num_classes
    str = sprintf('%s     %d', str, i);
end
for i=1:num_classes
    str = sprintf('%s\n%2d ', str, i);
    for j=1:num_classes
        str = sprintf('%s%5d ', str, cm(i,j));
    end
end
str = sprintf('%s\n', str);

% printf float CM here
str = sprintf('%s\nConfusion matrix (float, x:predicted label, y:true label):\n', str);
for i=1:num_classes
    str = sprintf('%s     %d', str, i);
end
for i=1:num_classes
    str = sprintf('%s\n%2d ', str, i);
    for j=1:num_classes
        str = sprintf('%s%.3f ', str, cm(i,j)/sum(cm(i,:)));
    end
end
str = sprintf('%s\n', str);

fprintf('%s',str);
