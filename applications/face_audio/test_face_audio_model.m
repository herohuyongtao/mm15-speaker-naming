clear;clc;close all;

addpath data/
addpath results/
addpath utils/

% data params
id = 7;
file_face_audio_model = sprintf('data/face_audio_model/face_audio_model_%d.mat', id);
file_test = 'data/extracted_features/face_audio_test_1v5.mat';
num_classes = 5;

% load model
clearvars -global config;
clearvars -global mem;
init_gpu(1);
global config;
load(file_face_audio_model);
weights = config.weights;
init_j_v5b();
config.weights = weights;

% load test data
load(file_test);
test_imgs = gpuArray(single(face));
test_audios = gpuArray(single(audio));
test_labels = gpuArray(single(label));
val_size = size(label, 2);

% ...
all_estimated_labels = [];
val_audios = gpuArray(single(zeros(size(test_audios, 1), config.batch_size)));
for v = 1:5
    print_progress(1,5,v,'Processing: ');
    outputs = zeros(config.output_size, val_size);
    for m = 1:val_size / config.batch_size
        val_in = test_imgs(:,:,:,(m-1)*config.batch_size+1:m*config.batch_size);                
        val_audios = test_audios(:, (v-1)*val_size + (m-1)*config.batch_size+1 : (v-1)*val_size + m*config.batch_size);

        out = compute_output_v5_double_in(val_in, val_audios);
        
        outputs(:, (m-1)*config.batch_size+1:m*config.batch_size) = gather(out);
    end
    [max_val, estimated_labels] = max(outputs);
    
    all_estimated_labels = [all_estimated_labels estimated_labels];
end

% print accuracy
[max_val, true_labels] = max(test_labels);
labels_all = kron(true_labels,ones(5,1))';  % element-wise 5x copy
labels_all = labels_all(:)';                % element-wise 5x copy
str = print_accuracy(all_estimated_labels, labels_all, num_classes);

% save accuracy info to txt
fileID = fopen(sprintf('C:/%d.txt', id), 'w');
fprintf(fileID, '%s', str);
fclose(fileID);