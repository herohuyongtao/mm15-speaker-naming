clear;clc;close all;

addpath cuda/
addpath utils/
addpath data/

% data params
folder_face = 'data/faces';
folder_audio = 'data/extracted_features/audio-merged-456-3';
folder_to_save = 'data/face_audio_model';
str_train_prefix = 'train_';
idx_range_train_submat_face = [1 15];
file_face_alone_model = 'data/face_model/face_model.mat';
file_test = 'data/extracted_features/face_audio_test_456v3.mat';
num_classes = 5;
num_passes = 30; % will save a model at end of each pass 

% training
clearvars -global config;
clearvars -global mem;
global config;
init_gpu(1);
load(file_face_alone_model);
weights = config.weights;
init_j_v5b();
config.weights = weights;

% load training audio data
load(sprintf('%s/audio_samples_train_75_20ms.mat', folder_audio));
r = 1;
new_weights = randn(size(config.weights.W1, 2), size(sample, 1)) * r;
new_weights = new_weights / sqrt(size(sample, 1));
config.weights.W1 = cat(2, config.weights.W1, new_weights);

audio_sample_cell = cell(1, num_classes);
[max_val, train_aud_labels] = max(tag);
for i=1:num_classes
    idx = find(train_aud_labels == i);
    audio_sample_cell{i} = gpuArray(single(sample(:, idx)));
end
clear sample tag; % to avoid affect variables with the same names 

% load test data
load(file_test);
test_imgs = gpuArray(single(face));
test_audios = gpuArray(single(audio));
test_labels = gpuArray(single(label));

learning_rate = 0.01;
decay = 5e-7 / 10;
points_seen = 0;
audio_in = gpuArray(single(zeros(size(audio_sample_cell{1}, 1), config.batch_size)));
for pass = 1:num_passes
	for p = idx_range_train_submat_face(1):idx_range_train_submat_face(2)
        load(sprintf('%s/%s%d.mat', folder_face, str_train_prefix, p));
		perm = randperm(size(sample, 4));
		sample = sample(:,:,:,perm);
		tag = tag(:,perm);
		
		train_imgs = gpuArray(single(sample));
		train_labels = gpuArray(single(tag));
		for i = 1:length(train_labels) / config.batch_size
			eta = learning_rate / (1 + points_seen*decay);
			points_seen = points_seen + config.batch_size;
			in = train_imgs(:,:,:,(i-1)*config.batch_size+1:i*config.batch_size);
			out = train_labels(:,(i-1)*config.batch_size+1:i*config.batch_size);
			
			[max_val, labels] = max(out);        
			for a = 1:length(labels)
				rn = randi(size(audio_sample_cell{labels(a)}, 2));
				audio_in(:, a) = audio_sample_cell{labels(a)}(:, rn);
			end
			
			[cost, grad] = core_actions_j_v5_double_in(in, audio_in, out);
			config.weights.C1 = config.weights.C1 - eta * grad.dC1 / sqrt((config.kernel_size(1, 1) * config.kernel_size(1, 2) * config.conv_hidden_size(1)));
			config.weights.C2 = config.weights.C2 - eta * grad.dC2 / sqrt((config.kernel_size(2, 1) * config.kernel_size(2, 2) * config.conv_hidden_size(2)));
			config.weights.C3 = config.weights.C3 - eta * grad.dC3 / sqrt((config.kernel_size(3, 1) * config.kernel_size(3, 2) * config.conv_hidden_size(3)));
			config.weights.S1 = config.weights.S1 - eta * grad.dS1 / 2;
			config.weights.S2 = config.weights.S2 - eta * grad.dS2 / 2;
			config.weights.W1 = config.weights.W1 - eta * grad.dW1 / sqrt(config.full_hidden_size(1));
			config.weights.W2 = config.weights.W2 - eta * grad.dW2 / sqrt(config.output_size);
			config.weights.bc1 = config.weights.bc1 - eta * grad.dbc1 / sqrt(config.pooling_layer_size(1, 1)*2*config.pooling_layer_size(1, 2)*2);
			config.weights.bc2 = config.weights.bc2 - eta * grad.dbc2 / sqrt(config.pooling_layer_size(2, 1)*2*config.pooling_layer_size(2, 2)*2);
			config.weights.bc3 = config.weights.bc3 - eta * grad.dbc3 / sqrt(config.conv_hidden_size(3));
			config.weights.bs1 = config.weights.bs1 - eta * grad.dbs1 / sqrt(config.pooling_layer_size(1, 1) * config.pooling_layer_size(1, 2));
			config.weights.bs2 = config.weights.bs2 - eta * grad.dbs2 / sqrt(config.pooling_layer_size(2, 1) * config.pooling_layer_size(2, 2));
			config.weights.bf1 = config.weights.bf1 - eta * grad.dbf1 / sqrt(config.full_hidden_size(1));
			config.weights.bf2 = config.weights.bf2 - eta * grad.dbf2 / sqrt(config.output_size);
        end
    end
    
    % save model
	fprintf('saving %s...\n\n', sprintf('c:/face_audio_model_%d.mat', pass));
	save(sprintf('%s/face_audio_model_%d.mat', folder_to_save, pass), 'config');
    
    % save acc report
    val_size = size(test_labels, 2);
    all_estimated_labels = [];
    val_audios = gpuArray(single(zeros(size(test_audios, 1), config.batch_size)));
    for v = 1:5
        print_progress(1,5,v,'Testing: ');
        outputs = zeros(config.output_size, val_size);
        for m = 1:val_size / config.batch_size
            val_in = test_imgs(:,:,:,(m-1)*config.batch_size+1:m*config.batch_size);                
            val_audios = test_audios(:, (v-1)*val_size + (m-1)*config.batch_size+1 : (v-1)*val_size + m*config.batch_size);

            out = compute_output_v5_double_in(val_in, val_audios);

            outputs(:, (m-1)*config.batch_size+1:m*config.batch_size) = gather(out);
        end
        [max_val, estimated_labels] = max(outputs);

        all_estimated_labels = [all_estimated_labels estimated_labels];
    end
    [max_val, true_labels] = max(test_labels);
    labels_all = kron(true_labels,ones(5,1))';  % element-wise 5x copy
    labels_all = labels_all(:)';                % element-wise 5x copy
    str = print_accuracy(all_estimated_labels, labels_all, num_classes);
    fileID = fopen(sprintf('%s/face_audio_model_acc_%d.txt', folder_to_save, pass), 'w');
    fprintf(fileID, '%s', str);
    fclose(fileID);
end
