function numgrad = computeNumericalGradient_double_in(image, audios, label, estimatedGrad)
    global config;
    epsilon = gsingle(0.01);
    % Initialize numgrad with zeros
    numgrad = zeros(size(config.weights.C1));
    %{
    % % % % % % % % % % % % % % % %
    N = size(config.weights.C1, 1) * size(config.weights.C1, 2);
    try % Initialization
       ppm = ParforProgressStarter2('test', N, 0.1);
    catch me % make sure "ParforProgressStarter2" didn't get moved to a different directory
       if strcmp(me.message, 'Undefined function or method ''ParforProgressStarter2'' for input arguments of type ''char''.')
           error('ParforProgressStarter2 not in path.');
       else
           % this should NEVER EVER happen.
           msg{1} = 'Unknown error while initializing "ParforProgressStarter2":';
           msg{2} = me.message;
           print_error_red(msg);
           % backup solution so that we can still continue.
           ppm.increment = nan(1, nbr_files);
       end
    end
    % % % % % % % % % % % % % % % %
    %}
    for x = 1:size(config.weights.C1, 1)
        for y = 1:size(config.weights.C1, 2)
            config.weights.C1(x, y) = config.weights.C1(x, y) + epsilon;
            [cost1, grad] = core_actions_j_v5_double_in(image, audios, label);
            config.weights.C1(x, y) = config.weights.C1(x, y)  - (2*epsilon);
            [cost2, grad] = core_actions_j_v5_double_in(image, audios, label);
            config.weights.C1(x, y) = config.weights.C1(x, y) + epsilon;
            
            % compute the numerical 
            temp = (cost1 - cost2) / (2 * epsilon);
            numgrad(x, y) = temp;

            diff = norm(temp-estimatedGrad.dC1(x, y));
            threshold = 10^-10;
            %if(diff > threshold)
                fprintf('numerical: %i, ', temp);
                fprintf('estimated: %i. \n', estimatedGrad.dC1(x, y));
                fprintf('loop (%d, %d)\n', x, y);
                fprintf('diff too large! diff: %i.\n', diff);
            %end
            %ppm.increment((x-1)*size(config.weights.W2, 1)+y);
        end
    end
    
    try % use try / catch here, since delete(struct) will raise an error.
       delete(ppm);
    catch me %#ok<NASGU>
    end
end



