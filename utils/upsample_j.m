function ret = upsample_j(obj, target)
    m = gones(target);
    ret = kron(obj, m);
end

